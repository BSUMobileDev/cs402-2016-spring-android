package com.zstudiolabs.bsulocations;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.util.ArrayList;

public class WebServices {

    private static final String HOST_URL = "http://zstudiolabs.com";
    private static final String WS_GET_LOCATIONS = "/labs/cs402/buildings.json";

    private static Boolean isRunning = false;

    private static GetResourceTask getResourceTask;

    private static WebServices webServicesInstance = new WebServices();

    private WebServices()
    {
        // Disallow this
    }

    public static WebServices getInstance()
    {
        return webServicesInstance;
    }

    public void refreshLocations()
    {
        if( isRunning )
        {
            return;
        }

        isRunning = true;
        getResourceTask = new GetResourceTask();
        getResourceTask.execute();
    }
    public void saveLocationsToServer( ArrayList<LocationModel> locationsArray)
    {
        if( isRunning )
        {
            return;
        }
        // Do a POST
    }

    public class GetResourceTask extends AsyncTask<String, Integer, String>
    {
        public static final int TIMEOUT = 30000;

        public GetResourceTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String resourceURL = HOST_URL + WS_GET_LOCATIONS;

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

            HttpGet httpGet = new HttpGet(resourceURL);

            String response = "";
            try {
                BasicResponseHandler responseHandler = new BasicResponseHandler();
                response = httpClient.execute(httpGet, responseHandler);

                Log.i("MZ", response);
            }
            catch ( Exception exception)
            {
                Log.e("MZ", exception.toString());
            }

            Log.i("MZ", "Inside Do In Background: " + resourceURL);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            LocationsFactory.createLocationsFromJSON(response);

            isRunning = false;
        }
    }
}
