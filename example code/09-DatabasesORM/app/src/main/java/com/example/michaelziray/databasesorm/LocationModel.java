package com.example.michaelziray.databasesorm;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Locations")
public class LocationModel extends Model {

//    @Column(name = "coordinates")
//    public LatLng locationCoordinates;

    @Column(name = "location_name")
    String locationName;

    @Column(name = "location_description")
    public String locationDescription;

//    public LatLng getLocationCoordinates() {
//        return locationCoordinates;
//    }
//
//    public void setLocationCoordinates(LatLng locationCoordinates) {
//        this.locationCoordinates = locationCoordinates;
//    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }
}
