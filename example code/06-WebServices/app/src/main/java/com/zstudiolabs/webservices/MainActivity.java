package com.zstudiolabs.webservices;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {

    public static final String HOST_URL = "https://www.bitstamp.net";
    Button downloadButton;
    String currentBidPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        downloadButton = (Button)findViewById(R.id.button);

        downloadButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetResourceTask getResourceTask = new GetResourceTask(HOST_URL);
                getResourceTask.execute("/api/ticker/");
            }
        });
    }

    public class GetResourceTask extends AsyncTask<String, Integer, Boolean>
    {
        public static final int TIMEOUT = 30000;
        String hostURL;

        public GetResourceTask(String newHostURL) {
            super();
            this.hostURL = newHostURL;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            downloadButton.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String resourceURL = hostURL + params[0];

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

            HttpGet httpGet = new HttpGet(resourceURL);

            try {
                BasicResponseHandler responseHandler = new BasicResponseHandler();
                String response = httpClient.execute(httpGet, responseHandler);

                JSONObject jsonObject = new JSONObject(response);
                currentBidPrice = jsonObject.getString("bid");

                Log.i("TAG", response);
            }
            catch ( Exception exception)
            {
                Log.e("TAG", exception.toString());

            }

            Log.i("TAG", "Inside Do In Background: " + resourceURL);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            TextView bidpriceTextView = (TextView)findViewById(R.id.textView);
            bidpriceTextView.setText(getString( R.string.money_symbol ) + currentBidPrice);

            downloadButton.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
