package com.zstudiolabs.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
        Log.i("MZ", "fragment constructor was called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("MZ", "on create view was called");

        Toast.makeText(getActivity(), "This is a toast", Toast.LENGTH_LONG).show();

        return inflater.inflate(R.layout.fragment_main, container, false);
    }
}
