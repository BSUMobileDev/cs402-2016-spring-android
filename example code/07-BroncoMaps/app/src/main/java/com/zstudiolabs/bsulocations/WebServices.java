package com.zstudiolabs.bsulocations;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by michael.ziray on 2/19/15.
 */
public class WebServices {

    private static final String HOST_URL = "http://zstudiolabs.com";
    private static final String WS_GET_LOCATIONS = "/labs/cs402/buildings.json";

    private static Boolean isRunning = false;

    private WebServices()
    {
        // Disallow this
    }


    public static void refreshLocations(Context context)
    {
        if( isRunning )
        {
            return;
        }

        isRunning = true;
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = HOST_URL + WS_GET_LOCATIONS;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        LocationsFactory.createLocationsFromJSON(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handler
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
