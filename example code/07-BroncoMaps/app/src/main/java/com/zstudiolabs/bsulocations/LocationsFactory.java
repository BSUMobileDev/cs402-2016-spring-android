package com.zstudiolabs.bsulocations;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 2/19/15.
 */
public class LocationsFactory {

    public static final String JSON_LOCATION_NAME = "name";
    public static final String JSON_LOCATION_DESCRIPTION = "description";
    public static final String JSON_LOCATION_GPS = "location";

    public static ArrayList createLocationsFromJSON(String locationsJSON){
        ArrayList<LocationModel> locationsArrayList = new ArrayList<>();

        try {
            JSONArray jsonLocationsArray = new JSONArray(locationsJSON);

//            {
//                "name":"Bronco Stadium",
//                "location": { "latitude":43.60278575, "longitude":-116.1958694 },
//                "description":"Blue turf"
//            }
            for (int index = 0; index < jsonLocationsArray.length(); index++) {
                JSONObject locationJSON = (JSONObject)jsonLocationsArray.get(index);

                LocationModel newLocation = createLocationFromJSON(locationJSON);

                locationsArrayList.add(newLocation);
            }

            LocationsController.addLocation(locationsArrayList);
        }
        catch (Exception exception)
        {
            Log.e("TAG", exception.toString());
        }
        return locationsArrayList;
    }

    public static LocationModel createLocationFromJSON(JSONObject locationJSON) throws JSONException {
        String locationName = locationJSON.getString(JSON_LOCATION_NAME);
        String locationDescription = locationJSON.getString(JSON_LOCATION_DESCRIPTION);

        JSONObject locationGPS = (JSONObject)locationJSON.getJSONObject(JSON_LOCATION_GPS);
        double latitude = locationGPS.getDouble("latitude");
        double longitude = locationGPS.getDouble("longitude");
        LatLng latLng = new LatLng(latitude, longitude);

        LocationModel newLocation = new LocationModel();
        newLocation.setLocationName(locationName);
        newLocation.setLocationDescription(locationDescription);
        newLocation.setLocationCoordinates(latLng);
        return newLocation;
    }
}
