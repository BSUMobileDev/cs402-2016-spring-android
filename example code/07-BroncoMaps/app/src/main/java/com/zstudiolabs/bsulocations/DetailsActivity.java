package com.zstudiolabs.bsulocations;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.otto.Subscribe;


public class DetailsActivity extends ActionBarActivity {

    private Button cameraButton;
    private Button saveButton;
    private ImageView imageView;
    private EditText nameEditText;
    private EditText descriptionEditText;

    private Bitmap imageBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        nameEditText = (EditText)findViewById(R.id.nameText);
        descriptionEditText = (EditText)findViewById(R.id.descriptionText);

        LocationsController.startLocationReporting(this);

        cameraButton = (Button)findViewById(R.id.cameraButton);
        saveButton = (Button)findViewById(R.id.saveButton);

        cameraButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 0);
            }
        });

        saveButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Save data from interface
                String locationName = nameEditText.getText().toString();
                String locationDescription = descriptionEditText.getText().toString();
                Location currentLocation = LocationsController.getCurrentLocation();

                // compile location object
                LocationModel newLocationModel = new LocationModel();
                newLocationModel.setLocationName( locationName );
                newLocationModel.setLocationDescription(locationDescription);
                newLocationModel.setLocationCoordinates( new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()) );
                newLocationModel.setLocationImage( imageBitmap );
                LocationsController.addLocation( newLocationModel );
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // save camera data
        Bundle extras = data.getExtras();
        imageBitmap = (Bitmap) extras.get("data");
        imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 1000, 10000, false);
        ImageView imageView = (ImageView)findViewById(R.id.imageView);
        imageView.setImageBitmap(imageBitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
