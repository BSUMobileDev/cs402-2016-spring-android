package com.zstudiolabs.bsulocations;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 2/19/15.
 */
public class LocationsController {

    static ArrayList<LocationModel> locationsArray = new ArrayList<LocationModel>();

    private static Location location;

    public static void addLocation(LocationModel newLocation)
    {
        locationsArray.add( newLocation );

        // Broadcast that a pin was added
        BusProvider.getInstance().post(new AddLocationEvent());
    }

    public static void addLocation(ArrayList<LocationModel> newLocationsArray)
    {
        locationsArray.addAll( newLocationsArray );

        // Broadcast that a pin was added
        BusProvider.getInstance().post(new AddLocationEvent());
    }

    public static ArrayList<LocationModel> getLocations()
    {
        return locationsArray;
    }

    public static Boolean removeLocation(LocationModel locationToRemove)
    {
        // For loop to search array and remove this item
        return locationsArray.remove(locationToRemove);

        // Broadcast that the pin was removed
    }

    public static void startLocationReporting(Context context) {
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("TAG", "location: " + location);
                LocationsController.location = location;
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    public static Location getCurrentLocation()
    {
        return location;
    }
}
